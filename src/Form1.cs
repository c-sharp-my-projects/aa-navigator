﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using WindowsFormsApplication372;
using System.IO;
using System.Linq;
using WindowsFormsApplication1;
using System.Threading;
using System.Runtime.InteropServices;
using HotKey_ns;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string file = "table_locations";

        static private Point screen_search(string path)
        {
            Template template;
            Bitmap source;
            Point? foundPoint;
            //загружаем изображение
            source = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            Graphics graphics = Graphics.FromImage(source as Image);
            graphics.CopyFromScreen(0, 0, 0, 0, source.Size);

            //уст стандартное DPI
            source.SetResolution(96, 96);

            //берем фрагмент из большого изображения
            var fragment = new Bitmap(@path);

            //создаем шаблон
            template = new Template(fragment);

            //поиск точки
            foundPoint = template.Find(source);
            //центр найденного фрагмента
            Point center = new Point(foundPoint.Value.X + fragment.Width / 2, foundPoint.Value.Y + fragment.Height / 2);

            return center;
        }


        Point sdvig = new Point();

        private void button1_Click(object sender, EventArgs e)
        {
            if (upgrade_sdvig())
            {
                Point target = new Point();                
                target.X = Convert.ToInt16(dataGridView1.SelectedRows[0].Cells["xcoord"].Value)- sdvig.X;
                target.Y = Convert.ToInt16(dataGridView1.SelectedRows[0].Cells["ycoord"].Value)- sdvig.Y;
                Cursor.Position = target;
            }

            //world_map 643,336
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            form2 = new Form();
            label_massage = new Label();
            form2.Controls.Add(label_massage);

            this.Size = new Size(305, 601);

            tableInit();
            this.Location = Properties.Settings.Default.wLoc;
            if (Properties.Settings.Default.file_name != "") file = Properties.Settings.Default.file_name;
            load();
            bind_find_start_loc();
            bind_add();
            bind_painting();
            bind_find();
            next_point();
            sort();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (upgrade_sdvig())
            {
                dataGridView1.Rows.Add("", Cursor.Position.X + sdvig.X, Cursor.Position.Y + sdvig.Y);
            }
        }

        void tableInit()
        {
            dataGridView1.Rows.Clear();
        }

        void save()
        {
            //DialogResult dlg = MessageBox.Show("Save?", "Save?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //if (dlg == DialogResult.Yes)
            //{
                List<string> list = new List<string>();
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    if (dataGridView1.Rows[i].IsNewRow == true) continue;
                    string loc_name = dataGridView1.Rows[i].Cells["location"].Value?.ToString()??"";
                    int xcoord = Convert.ToInt32(dataGridView1.Rows[i].Cells["xcoord"].Value);
                    int ycoord = Convert.ToInt32(dataGridView1.Rows[i].Cells["ycoord"].Value);
                    list.Add(loc_name+";"+ xcoord + ";" + ycoord);
                }
                File.WriteAllLines(file, list);
            //}
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            save();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {

            if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            file = openFileDialog1.FileName;
            load(file);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.wLoc = this.Location;
            Properties.Settings.Default.file_name = file;
            Properties.Settings.Default.Save();
        }

        void load()
        {
            try
            {
                dataGridView1.Rows.Clear();
                string[] list = File.ReadAllLines(file);
                foreach (var item in list)
                {
                    dataGridView1.Rows.Add(item.Split(';'));
                }
            }
            catch { File.Create(file); }
            

        }

        void load(string dialog_gile)
        {
            file = dialog_gile;
            dataGridView1.Rows.Clear();
            string[] list = File.ReadAllLines(file);
            foreach (var item in list)
                {
                    dataGridView1.Rows.Add(item.Split(';'));
                }
        }

        Form form2;
        Label label_massage;
        private void show_message(string message)
        {
            timer1.Enabled = false;
            form2.BackColor = Color.Black;
            form2.TransparencyKey = Color.Black;
            form2.FormBorderStyle = FormBorderStyle.None;
            form2.TopMost = true;            
            Size resolution = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Size;
            form2.Height = Convert.ToInt32(resolution.Height * 0.1);
            form2.Width = Convert.ToInt32(resolution.Width * 0.5);
            form2.StartPosition = FormStartPosition.CenterScreen;
            form2.Visible = true;
            label_massage.Height = form2.Height;
            label_massage.Width = form2.Width;
            label_massage.Location = new Point(0, 0);
            Font fnt = new Font("Arial", Convert.ToInt32(form2.Height * 0.6), GraphicsUnit.Point);
            label_massage.Font = fnt;
            label_massage.TextAlign = ContentAlignment.MiddleCenter;
            label_massage.ForeColor = Color.Red;            
            label_massage.Text = message;
            timer1.Interval = 2000;
            form2.Show();
            timer1.Enabled = true;
                      
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (upgrade_sdvig())
            {
                Console.WriteLine(Cursor.Position.X + ";" + Cursor.Position.Y);
                textBox1.Text = (Cursor.Position.X + sdvig.X).ToString();
                textBox2.Text = (Cursor.Position.Y + sdvig.Y).ToString();
            }
        }

        private double distance(int x1, int y1, int x2, int y2)
        {
            return Math.Sqrt(Math.Pow(x2 - x1, 2) + Math.Pow(y2 - y1, 2));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            save();
            dataGridView1.Rows.Clear();
            string[] list = File.ReadAllLines(file);
            string[] first_dot = new[] {";"+textBox1.Text+ ";" + textBox2.Text};
            
            string[] result_list = new string[list.Length+1];            

            int kostil = 1;
            if (list.Contains(first_dot[0])) { kostil=2; result_list = new string[list.Length]; }
            first_dot.CopyTo(result_list, 0);
            for (int i=0;i< result_list.Length-1; i++)
            {
                Dictionary<string, double> distances = new Dictionary<string, double>();
                foreach (var item2 in list)
                {                   
                    
                        if (result_list[i] != item2 && !result_list.Contains(item2))
                        {
                            int x1 = Convert.ToInt32(result_list[i].Split(';')[1]);
                            int y1 = Convert.ToInt32(result_list[i].Split(';')[2]);
                            int x2 = Convert.ToInt32(item2.Split(';')[1]);
                            int y2 = Convert.ToInt32(item2.Split(';')[2]);
                            distances.Add(item2, distance(x1, y1, x2, y2));
                        }
                    }
                    var keyAndValue = distances.OrderBy(kvp => kvp.Value).First();
                    result_list[i+1] = keyAndValue.Key; 
                }              

            foreach (var item in result_list)
            {
                if (kostil == 1)
                {
                    if (item != result_list[0])
                    {
                        dataGridView1.Rows.Add(item.Split(';'));
                    }
                } else { dataGridView1.Rows.Add(item.Split(';')); }
            }


        }

        private bool upgrade_sdvig()
        {
            try
            {
                string path = @"img\map2.png";
                Point world_map = new Point();
                world_map = screen_search(path);
                //sdvig.X = 643 - world_map.X;
                //sdvig.Y = 336 - world_map.Y;
                sdvig.X = 641 - world_map.X;
                sdvig.Y = 335 - world_map.Y;
                return true;
            }
            catch
            {
                try
                {
                    string path = @"img\telega.png";
                    Point world_map = new Point();
                    world_map = screen_search(path);                    
                    right_click_abs(world_map.X - 300, world_map.Y - 200);
                    right_click_abs(world_map.X - 300, world_map.Y - 200);
                    right_click_abs(world_map.X - 300, world_map.Y - 200);
                    //right_click_abs(world_map.X - 300, world_map.Y - 200);
                    try
                    {
                        path = @"img\map2.png";
                        world_map = new Point();
                        world_map = screen_search(path);
                        sdvig.X = 641 - world_map.X;
                        sdvig.Y = 335 - world_map.Y;
                        return true;
                    }
                    catch { /*MessageBox.Show("Не открыта карта"); */return false; }
                }
                catch { 
                    ssend("M");
                    Thread.Sleep(100);
                    try
                    {
                        string path = @"img\telega.png";
                        Point world_map = new Point();
                        world_map = screen_search(path);
                        right_click_abs(world_map.X - 300, world_map.Y - 200);
                        right_click_abs(world_map.X - 300, world_map.Y - 200);
                        right_click_abs(world_map.X - 300, world_map.Y - 200);
                        //right_click_abs(world_map.X - 300, world_map.Y - 200);
                        try
                        {
                            path = @"img\map2.png";
                            world_map = new Point();
                            world_map = screen_search(path);
                            sdvig.X = 641 - world_map.X;
                            sdvig.Y = 335 - world_map.Y;
                            return true;
                        }
                        catch { /*MessageBox.Show("Не открыта карта"); */return false; }
                    }
                    catch
                    {/* MessageBox.Show("Не открыта карта"); */
                        return false;
                    }
                    /* MessageBox.Show("Не открыта карта"); */
                    return false; }
            }
        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (upgrade_sdvig())
            {
                ssend_down("LMenu");
                ssend_down("Tab");
                ssend_up("Tab");
                ssend_up("LMenu");
                Thread.Sleep(rnd.Next(100, 150));                
                //left_click_abs(1566 - sdvig.X, 750 - sdvig.Y);

                int start_dot = 0;
                try { Console.WriteLine(dataGridView1.SelectedRows[0].Cells["xcoord"].Value); start_dot = dataGridView1.SelectedRows[0].Index; } catch { start_dot = 0; left_click_abs(1566 - sdvig.X, 750 - sdvig.Y); }

                if ((Convert.ToInt32(textBox1.Text) - sdvig.X) == (Convert.ToInt32(dataGridView1.Rows[start_dot].Cells["xcoord"].Value) - sdvig.X) &&
                    (Convert.ToInt32(textBox2.Text) - sdvig.Y) == (Convert.ToInt32(dataGridView1.Rows[start_dot].Cells["ycoord"].Value) - sdvig.Y))
                {
                    start_dot++;
                }

                left_click_abs(Convert.ToInt32(textBox1.Text) - sdvig.X, Convert.ToInt32(textBox2.Text) - sdvig.Y);

                int last_item_for_painting = dataGridView1.Rows.Count - 1;
                if (dataGridView1.Rows.Count - 1 - start_dot > 20) { last_item_for_painting = 20 - start_dot; dataGridView1.Rows[last_item_for_painting].Selected = true; }
                for (int i = start_dot; i < last_item_for_painting; i++)
                {
                    if (dataGridView1.Rows[i].IsNewRow == true) continue;
                    left_click_abs(Convert.ToInt32(dataGridView1.Rows[i].Cells["xcoord"].Value) - sdvig.X, Convert.ToInt32(dataGridView1.Rows[i].Cells["ycoord"].Value) - sdvig.Y);
                } 
            }
        }


        enum SystemMetric
        {
            SM_CXSCREEN = 0,
            SM_CYSCREEN = 1,
        }

        [DllImport("user32.dll")]
        static extern int GetSystemMetrics(SystemMetric smIndex);

        static int CalculateAbsoluteCoordinateX(int x)
        {
            return (x * 65536) / GetSystemMetrics(SystemMetric.SM_CXSCREEN);
        }

        static int CalculateAbsoluteCoordinateY(int y)
        {
            return (y * 65536) / GetSystemMetrics(SystemMetric.SM_CYSCREEN);
        }

        Random rnd = new Random();
        public void mouse_move_abs(int x, int y) { MouseSimulator.absMouseMove(CalculateAbsoluteCoordinateX(x), CalculateAbsoluteCoordinateY(y)); }
        public void left_click_down_abs(int x, int y) { mouse_move_abs(x, y); Thread.Sleep(rnd.Next(50, 100)); MouseSimulator.ClickLeftMouseButton_Down(); }
        public void left_click_up_abs(int x, int y) { mouse_move_abs(x, y); Thread.Sleep(rnd.Next(50, 100)); MouseSimulator.ClickLeftMouseButton_Up(); }
        public void left_click_abs(int x, int y) { left_click_down_abs(x, y); Thread.Sleep(rnd.Next(50,100)); left_click_up_abs(x, y); Thread.Sleep(rnd.Next(50, 100)); }
        public void ssend(string name_of_key) { ssend_down(name_of_key); Thread.Sleep(100); ssend_up(name_of_key); }
        public void ssend_down(string name_of_key) { KeyBoardSimulator.SendSpecialKey((int)ConvertFromString(name_of_key.Replace(" ", "")), KeyBoardSimulator.KEYEVENTF.KeyDown | KeyBoardSimulator.KEYEVENTF.SCANCODE); }
        public void ssend_up(string name_of_key) { KeyBoardSimulator.SendSpecialKey((int)ConvertFromString(name_of_key.Replace(" ", "")), KeyBoardSimulator.KEYEVENTF.KEYUP | KeyBoardSimulator.KEYEVENTF.SCANCODE); }
        public void right_click_down_abs(int x, int y) { mouse_move_abs(x, y); Thread.Sleep(rnd.Next(50, 100)); MouseSimulator.ClickRightMouseButton_Down(); }
        public void right_click_up_abs(int x, int y) { mouse_move_abs(x, y); Thread.Sleep(rnd.Next(50, 100)); MouseSimulator.ClickRightMouseButton_Up(); }
        public void right_click_abs(int x, int y) { right_click_down_abs(x, y); Thread.Sleep(rnd.Next(50, 100)); right_click_up_abs(x, y); Thread.Sleep(rnd.Next(50, 100)); }
        public static Keys ConvertFromString(string keystr)
        {
            return (Keys)Enum.Parse(typeof(Keys), keystr);
        }
        private static ushort key(string name)
        {
            var dict = new Dictionary<string, ushort>
            {{"`",0x29},{"1",0x02},{"2",0x03 },{"3",0x04 },{"4",0x05 },{"5",0x06 },{"6",0x07 },{"7",0x08 },{"8",0x09 },{"9",0x0A },{"0",0x0B },{"-",0x0C },{"=",0x0D },{"Tab",0x0F },{"q",0x10 },{"w",0x11 },{"e",0x12 },{"r",0x13 },{"t",0x14 },{"y",0x15 },{"u",0x16 },{"i",0x17 },{"o",0x18 },{"p",0x19 },{"[",0x1A },{"]",0x1B },{"a",0x1E },{"s",0x1F },{"d",0x20 },{"f",0x21 },{"g",0x22 },{"h",0x23 },{"j",0x24 },{"k",0x25 },{"l",0x26 },{";",0x27 },{"'",0x28 },{"\\",0x2B},{"z",0x2C },{"x",0x2D },{"c",0x2E },{"v",0x2F },{"b",0x30 },{"n",0x31 },{"m",0x32 },{",",0x33 },{".",0x34 },{"/",0x35 },{"f1",0x3b },{"f2",0x3c },{"f3",0x3d },{"f4",0x3e },{"f5",0x3f },{"f6",0x40 },{"f7",0x41 },{"f8",0x42 },{"f9",0x43 },{"f10",0x44 },{"f11",0x57 },{"f12",0x58 },{"Scroll lock",0x46 },{"esc",0x01 },{"return",0x1c },{"enter",0x1c },{"space",0x39 },{" ",0x39 },{"insert",0xd2 },{"delete",0xd3 },{"backspace",0x0e },{"left",0xcb },{"right",0xcd },{"up",0xc8 },{"down",0xd0 },{"home",0xc7 },{"end",0xcf },{"page up",0xc9 },{"page down",0xd1 },{"Numlock",0x45 },{"Num_=,",0xd },{"Num_/",0xb5 },{"Num_*",0x37 },{"Num_-",0x4a },{"Num_+",0x4e },{"Num_.",0x53 },{"Num_0",0x52 },{"Num_1",0x4f },{"Num_2",0x50 },{"Num_3",0x51 },{"Num_4",0x4b },{"Num_5",0x4c },{"Num_6",0x4d },{"Num_7",0x47 },{"Num_8",0x48 },{"Num_9",0x49 },{"Num_enter",0x9c },{"а", 0x21 },{"б", 0x33 },{"в", 0x20 },{"г", 0x16 },{"д", 0x26 },{"е", 0x14 },{"ё", 0x29},{"ж", 0x27 },{"з", 0x19 },{"й", 0x10 },{"и", 0x30 },{"к", 0x13 },{"л", 0x25 },{"м", 0x2F },{"н", 0x15 },{"о", 0x24 },{"п", 0x22 },{"р", 0x23 },{"с", 0x2E },{"т", 0x31 },{"у", 0x12 },{"ф", 0x1E},{"х", 0x1A },{"ц", 0x11 },{"ч", 0x2D },{"ш", 0x17 },{"щ", 0x18 },{"ъ", 0x1B },{"ы", 0x1F },{"ь", 0x32 },{"э", 0x28 },{"ю", 0x34 },{"я", 0x2C }};

            return dict[name];
        }

        private void bind_find_start_loc()
        {
            var hkey = new HotKey(Keys.Q, KeyModifiers.Control);
            hkey.Pressed += (o, e) =>
            {
                if (upgrade_sdvig())
                {
                    //Console.WriteLine(Cursor.Position.X + ";" + Cursor.Position.Y);
                    textBox1.Text = (Cursor.Position.X + sdvig.X).ToString();
                    textBox2.Text = (Cursor.Position.Y + sdvig.Y).ToString();                    
                }
                //hkey.Unregister();
            };
            hkey.Register(this);
        }

        private void bind_add()
        {
            var hkey = new HotKey(Keys.T, KeyModifiers.Control);
            hkey.Pressed += (o, e) =>
            {
                if (upgrade_sdvig())
                {
                    dataGridView1.CurrentCell = null;
                    dataGridView1.Rows.Add("", Cursor.Position.X + sdvig.X+1, Cursor.Position.Y + sdvig.Y+1);
                    if (dataGridView1.Rows[dataGridView1.Rows.Count - 1].IsNewRow == true) dataGridView1.Rows[dataGridView1.Rows.Count - 2].Selected = true;
                    else { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Selected = true; }
                    
                }
                //hkey.Unregister();
            };
            hkey.Register(this);
        }

        private bool int_in_textbox(TextBox textBox)
        {
            if (textBox.Text.Replace(" ", "") != "")
            {
                try
                {
                    Convert.ToInt32(textBox.Text);
                    return true;
                }
                catch { return true; }
            }
            else return false;
        }
        private void bind_painting()
        {            
            var hkey = new HotKey(Keys.W, KeyModifiers.Control);
            hkey.Pressed += (o, e) =>
            {
                try
                {
                    Console.WriteLine("index of raw now"+dataGridView1.SelectedRows[0].Index);
                }
                catch { Console.WriteLine("Null"); }
                if (upgrade_sdvig() /*&& int_in_textbox(textBox1) && int_in_textbox(textBox2)*/)
                {
                    left_click_abs(1566 - sdvig.X, 750 - sdvig.Y);
                    //left_click_abs(1566 - sdvig.X, 750 - sdvig.Y);                    

                    int start_dot = 0;
                    try { Console.WriteLine(dataGridView1.SelectedRows[0].Cells["xcoord"].Value); start_dot = dataGridView1.SelectedRows[0].Index; } 
                    catch { start_dot = 0; dataGridView1.Rows[0].Selected = true; try { left_click_abs(Convert.ToInt32(textBox1.Text) - sdvig.X, Convert.ToInt32(textBox2.Text) - sdvig.Y); } catch { } }

                    if (int_in_textbox(textBox1) && int_in_textbox(textBox2))
                    {
                        if ((Convert.ToInt32(textBox1.Text) - sdvig.X) == (Convert.ToInt32(dataGridView1.Rows[start_dot].Cells["xcoord"].Value) - sdvig.X) &&
                        (Convert.ToInt32(textBox2.Text) - sdvig.Y) == (Convert.ToInt32(dataGridView1.Rows[start_dot].Cells["ycoord"].Value) - sdvig.Y))
                        {
                            start_dot++;
                        }
                    }

                    int last_item_for_painting = dataGridView1.Rows.Count - 1;
                    //dataGridView1.CurrentCell = null;
                    if (dataGridView1.Rows.Count - 1 - start_dot > 19) { last_item_for_painting = start_dot + 18; /*dataGridView1.Rows[last_item_for_painting-1].Selected = true;*/ } 
                    /*else {
                        if (dataGridView1.Rows[dataGridView1.Rows.Count - 1].IsNewRow == true) dataGridView1.Rows[dataGridView1.Rows.Count - 2].Selected = true;
                        else { dataGridView1.Rows[dataGridView1.Rows.Count - 1].Selected = true; }
                    }*/
                    for (int i = start_dot; i < last_item_for_painting; i++)
                    {
                        if (dataGridView1.Rows[i].IsNewRow == true) continue;
                        left_click_abs(Convert.ToInt32(dataGridView1.Rows[i].Cells["xcoord"].Value) - sdvig.X, Convert.ToInt32(dataGridView1.Rows[i].Cells["ycoord"].Value) - sdvig.Y);
                    }
                    left_click_abs(1566 - sdvig.X, 750 - sdvig.Y);
                    if (last_item_for_painting == dataGridView1.RowCount - 1)
                    {
                        for(int i = 0; i < dataGridView1.SelectedRows.Count; i++)
                        {
                            dataGridView1.SelectedRows[i].Selected = false;
                        }
                    }
                }
                //hkey.Unregister();
            };
            hkey.Register(this);
        }

        private void bind_find()
        {
            var hkey = new HotKey(Keys.R, KeyModifiers.Control);
            hkey.Pressed += (o, e) =>
            {
                if (upgrade_sdvig())
                {                        
                        Point target = new Point();
                        target.X = Convert.ToInt16(dataGridView1.SelectedRows[0].Cells["xcoord"].Value) - sdvig.X;
                        target.Y = Convert.ToInt16(dataGridView1.SelectedRows[0].Cells["ycoord"].Value) - sdvig.Y;                        
                        ssend_down("LShiftKey");
                        left_click_abs(target.X, target.Y);
                        ssend_up("LShiftKey");
                    ssend("M");
                    show_message(dataGridView1.SelectedRows[0].Cells["location"].Value.ToString());
                }
                //hkey.Unregister();
            };
            hkey.Register(this);
        }

        private void next_point()
        {
            var hkey = new HotKey(Keys.E, KeyModifiers.Control);
            hkey.Pressed += (o, e) =>
            {
                if (upgrade_sdvig())
                {
                    int next_index = 0;
                    try { next_index = dataGridView1.SelectedRows[0].Index + 1; } catch { }
                    if (next_index != 0) { if (next_index > dataGridView1.Rows.Count - 1) { next_index = dataGridView1.Rows.Count - 1; } }
                    if (dataGridView1.Rows[next_index].IsNewRow == true) return;
                    dataGridView1.Rows[next_index].Selected = true;
                    Point target = new Point();
                    target.X = Convert.ToInt16(dataGridView1.SelectedRows[0].Cells["xcoord"].Value) - sdvig.X;
                    target.Y = Convert.ToInt16(dataGridView1.SelectedRows[0].Cells["ycoord"].Value) - sdvig.Y;
                    ssend_down("LShiftKey");
                    left_click_abs(target.X, target.Y);
                    ssend_up("LShiftKey");
                    ssend("M");
                    show_message(dataGridView1.SelectedRows[0].Cells["location"].Value.ToString());
                }
                //hkey.Unregister();
            };
            hkey.Register(this);
        }

        private void sort()
        {
            var hkey = new HotKey(Keys.Y, KeyModifiers.Control);
            hkey.Pressed += (o, e) =>
            {
                save();
                dataGridView1.Rows.Clear();
                string[] list = File.ReadAllLines(file);
                string[] first_dot = new[] { ";" + textBox1.Text + ";" + textBox2.Text };

                string[] result_list = new string[list.Length + 1];

                int kostil = 1;
                if (list.Contains(first_dot[0])) { kostil = 2; result_list = new string[list.Length]; }
                first_dot.CopyTo(result_list, 0);
                for (int i = 0; i < result_list.Length - 1; i++)
                {
                    Dictionary<string, double> distances = new Dictionary<string, double>();
                    foreach (var item2 in list)
                    {

                        if (result_list[i] != item2 && !result_list.Contains(item2))
                        {
                            int x1 = Convert.ToInt32(result_list[i].Split(';')[1]);
                            int y1 = Convert.ToInt32(result_list[i].Split(';')[2]);
                            int x2 = Convert.ToInt32(item2.Split(';')[1]);
                            int y2 = Convert.ToInt32(item2.Split(';')[2]);
                            distances.Add(item2, distance(x1, y1, x2, y2));
                        }
                    }
                    var keyAndValue = distances.OrderBy(kvp => kvp.Value).First();
                    result_list[i + 1] = keyAndValue.Key;
                }

                foreach (var item in result_list)
                {
                    if (kostil == 1)
                    {
                        if (item != result_list[0])
                        {
                            dataGridView1.Rows.Add(item.Split(';'));
                        }
                    }
                    else { dataGridView1.Rows.Add(item.Split(';')); }
                }
                //hkey.Unregister();
            };
            hkey.Register(this);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Console.WriteLine(dataGridView1.SelectedRows[0].Index);
            //this.dataGridView1.EndEdit();
        }

        Thread for_select_raw;

        private void func_for_thread()
        {
            //Console.WriteLine("222222");
            Thread.Sleep(100);            
            dataGridView1.Rows[index_of_raw_of_cell_that_was_edit].Selected = true;
            for_select_raw.Abort();
        }
        private void dataGridView1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {            
            for_select_raw = new Thread(new ThreadStart(func_for_thread));
            for_select_raw.Start();
            //Console.WriteLine("1111111");
        }

        int index_of_raw_of_cell_that_was_edit;
        private void dataGridView1_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {
            index_of_raw_of_cell_that_was_edit = e.RowIndex;
        }

        private void helpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Ctrl+Q - текущее положение \r\n Ctrl+W - отрисовать маршрут \r\n Ctrl+E - следующая точка маршрута \r\n Ctrl+R - текущая точка маршрута \r\n Ctrl+T - добавить точку в конец \r\n Ctrl+Y - сортировать список");
        }

        private void dataGridView1_RowPostPaint(object sender, DataGridViewRowPostPaintEventArgs e)
        {
            int index = e.RowIndex;
            string indexStr = (index + 1).ToString();
            object header = this.dataGridView1.Rows[index].HeaderCell.Value;
            if (header == null || !header.Equals(indexStr))
                this.dataGridView1.Rows[index].HeaderCell.Value = indexStr;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            form2.Hide();
            timer1.Enabled = false;
        }
    }
}
